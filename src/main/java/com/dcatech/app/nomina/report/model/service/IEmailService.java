package com.dcatech.app.nomina.report.model.service;

import com.dcatech.app.nomina.report.model.entity.Email;
import org.springframework.mail.MailException;

import javax.mail.MessagingException;

public interface IEmailService {
    void enviarMensaje(Email email);

    void enviarMensajeAadjunto(Email email) throws MailException, MessagingException;
}
