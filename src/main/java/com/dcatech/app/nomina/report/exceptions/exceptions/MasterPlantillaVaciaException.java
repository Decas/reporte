package com.dcatech.app.nomina.report.exceptions.exceptions;

public class MasterPlantillaVaciaException extends Exception {

    private static final String DESCRIPCION = "La plantilla no tiene conceptos asociados";

    public MasterPlantillaVaciaException() {
        super(DESCRIPCION);
    }
}
