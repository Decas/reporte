package com.dcatech.app.nomina.report.model.service.implement;


import com.dcatech.app.nomina.report.cliente.ILiquidacionFeing;
import com.dcatech.app.nomina.report.model.entity.Nomina;
import com.dcatech.app.nomina.report.model.service.IReportService;
import com.dcatech.nomina.db.commons.models.entity.DetalleLiquidacion;
import com.dcatech.nomina.db.commons.models.entity.Liquidacion;
import com.dcatech.nomina.db.commons.models.entity.PayingStatus;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static com.google.common.base.Preconditions.checkNotNull;

@Service
public class ReportService implements IReportService {


  @Autowired
    private ILiquidacionFeing liquidacionFeing;

     protected AbstractRoutingDataSource abstractRoutingDataSource;

/*
    @Override
    public String printReport(String format, Nomina nomina) throws JRException, FileNotFoundException, SQLException {

        checkNotNull(nomina.getClase(),"clase liquidación es requerida");
        checkNotNull(nomina.getPeriodo(),"Periodo es requerido");
        checkNotNull(nomina.getContratos(),"contrato es requerido");
        AtomicReference<Liquidacion> liquidaciones = new AtomicReference<>();

        ArrayList<Long> clases = new ArrayList<>();
        ArrayList<Long> contratos = new ArrayList<>();
        AtomicReference<Long> longAtomicReference = new AtomicReference<>();

        nomina.getClase().stream().forEach(detalleTipoReferencia -> {

            nomina.getContratos().stream().forEach(contrato -> {

//                  liquidaciones.set(this.liquidacionFeing.findByPeriodo(nomina.getPeriodo().getId(),
//                          detalleTipoReferencia.getId(), contrato.getId()));
//                lista.add(liquidaciones.get());

                longAtomicReference.set(nomina.getPeriodo().getId());
                clases.add(detalleTipoReferencia.getId());
                contratos.add(contrato.getId());

            });

        });
        System.out.println(longAtomicReference);
        System.out.println(clases);
        System.out.println(contratos);


        String path = "C:\\Nueva carpeta";
        //List<Liquidacion> employees = this.liquidacionList;

        Connection connection = null;

<<<<<<< HEAD
 */

    @Override
    public String printReport(Nomina nomina) throws JRException, FileNotFoundException, SQLException {

        checkNotNull(nomina.getClase(),"clase liquidación es requerida");
        checkNotNull(nomina.getPeriodo(),"Periodo es requerido");
        checkNotNull(nomina.getContratos(),"contrato es requerido");
        AtomicReference<Liquidacion> liquidaciones = new AtomicReference<>();

        ArrayList<Long> clases = new ArrayList<>();
        ArrayList<Long> contratos = new ArrayList<>();
        ArrayList<String> areas = new ArrayList<>();
        ArrayList<Long> empleados = new ArrayList<>();
        AtomicReference<Long> longAtomicReference = new AtomicReference<>();

        nomina.getClase().stream().forEach(detalleTipoReferencia -> {

            nomina.getContratos().stream().forEach(contrato -> {

//
                longAtomicReference.set(nomina.getPeriodo().getId());
                clases.add(detalleTipoReferencia.getId());
                areas.add(detalleTipoReferencia.getNombre());
                empleados.add(contrato.getEmpleado().getId());
                contratos.add(contrato.getId());

            });

        });
        System.out.println(longAtomicReference);
        System.out.println(clases);
        System.out.println(contratos);
        System.out.println(areas);
        System.out.println(empleados);

//creaar ruta
        String path = "C:\\Nueva carpeta";


        Connection connection = null;


        connection = DriverManager.getConnection("jdbc:postgresql://dbnominapp.cinhx37vh6l2.us-east-2.rds.amazonaws.com:5432/dbnomina_app_dev",
                "root", "Dcatech$2020");

        boolean valid = connection.isValid(50000);

        System.out.println(valid);
        //load file and compile it
        File file = ResourceUtils.getFile("C:\\Users\\kevin\\Downloads\\report.jrxml") ;
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        //JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employees);
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("createdBy", "DCA Technologies");

        parameters.put("periodo_id", longAtomicReference.get());
        parameters.put("contrato", contratos.iterator().next());
        parameters.put("referencia_id", clases.iterator().next());
        parameters.put("tipo_liquidacion", areas.iterator().next());



            JasperExportManager.exportReportToPdfFile(jasperPrint, path + "\\report.pdf");
            connection.close();


        return "succes";
    }
}

