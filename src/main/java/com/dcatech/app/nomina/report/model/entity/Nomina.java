package com.dcatech.app.nomina.report.model.entity;

import com.dcatech.nomina.db.commons.models.entity.Contrato;
import com.dcatech.nomina.db.commons.models.entity.DetalleTipoReferencia;

import com.dcatech.nomina.db.commons.models.entity.PeriodoAnioFiscal;
import lombok.Data;

import java.util.List;

@Data
public class Nomina {
    private PeriodoAnioFiscal periodo;
    private List<DetalleTipoReferencia> clase;
    private List<Contrato> contratos;

}
