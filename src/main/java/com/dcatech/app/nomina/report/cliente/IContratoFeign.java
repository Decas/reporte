package com.dcatech.app.nomina.report.cliente;

import com.dcatech.nomina.db.commons.models.entity.Contrato;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "nomina-master-service", url = "https://tljs21sni0.execute-api.us-east-2.amazonaws.com/nominapp/master")
public interface IContratoFeign {

    @GetMapping("/contratos/periodo/{id}/clase/{claseId}")
 List<Contrato> findByContratos(@PathVariable Long id, @PathVariable Long claseId);

}


