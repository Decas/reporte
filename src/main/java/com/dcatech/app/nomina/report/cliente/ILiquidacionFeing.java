 package com.dcatech.app.nomina.report.cliente;

import com.dcatech.nomina.db.commons.models.entity.Liquidacion;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "nomina-app-liquidacion", url = "https://tljs21sni0.execute-api.us-east-2.amazonaws.com/nominapp/liquidacion")
public interface ILiquidacionFeing {

    @GetMapping("/nomina/periodo/{id}/clase/{classId}/contrato/{contratoId}")
     Liquidacion findByPeriodo(@PathVariable Long id, @PathVariable Long classId, @PathVariable Long contratoId);

    }


