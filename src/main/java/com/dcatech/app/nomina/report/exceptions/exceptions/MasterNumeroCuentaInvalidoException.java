package com.dcatech.app.nomina.report.exceptions.exceptions;

public class MasterNumeroCuentaInvalidoException extends Exception {
    public static final String DESCRIPCION = "El numero de cuenta debe ser numérico.";

    public MasterNumeroCuentaInvalidoException() {
        super(DESCRIPCION);
    }
}
