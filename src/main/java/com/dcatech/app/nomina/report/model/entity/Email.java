package com.dcatech.app.nomina.report.model.entity;

import com.dcatech.nomina.db.commons.models.entity.Contrato;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Email {
    private List<Contrato> receptor;

    private String contenido;

    private String asunto;




}
