package com.dcatech.app.nomina.report.exceptions.exceptions;

public class MasterLiquidacionNoRevisadaExcception extends Exception {

    public static final String DESCRIPCION = "La liquidación no está revisada";

    public MasterLiquidacionNoRevisadaExcception() {
        super(DESCRIPCION);
    }

    public MasterLiquidacionNoRevisadaExcception(String message) {
        super(DESCRIPCION + ": " + message);
    }
}
