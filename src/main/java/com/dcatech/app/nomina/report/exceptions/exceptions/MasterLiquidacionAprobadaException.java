package com.dcatech.app.nomina.report.exceptions.exceptions;

public class MasterLiquidacionAprobadaException extends Exception{


    public static final String DESCRIPCION = "Liquidación ya fue aprobada";

    public MasterLiquidacionAprobadaException() {
        super(DESCRIPCION);
    }

    public MasterLiquidacionAprobadaException(String message) {
        super(DESCRIPCION + ": " + message);
    }
}
