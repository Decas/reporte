package com.dcatech.app.nomina.report.model.service.implement;

import com.dcatech.app.nomina.report.model.entity.Email;
import com.dcatech.app.nomina.report.model.service.IEmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@Service("IEmailService")
public class EmailService implements IEmailService {

    private static final String pathToAttachment="D://Nueva carpeta//report.pdf";

    @Autowired
    private JavaMailSender emailSender;

    @Override
    public void enviarMensajeAadjunto(Email email) throws  MailException,MessagingException {
        try{
            MimeMessage message = this.emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setFrom("decastillo@dcatech.com");
            helper.setText(email.getContenido());
            email.getReceptor().stream()
                    .forEach(contrato ->{
                        try {
                            helper.setTo(contrato.getEmpleado().getCorreoElectronico());
                        } catch (MessagingException e) {
                            e.printStackTrace();
                        }
                    } );
            helper.setSubject(email.getAsunto());
            FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
            helper.addAttachment("report.pdf",file);
            this.emailSender.send(message);


        }
        catch(MailException | MessagingException m){
         throw new MessagingException();
        }

    }

    @Override
    public void enviarMensaje(Email email) {
       try {

           SimpleMailMessage message = new SimpleMailMessage();

           message.setFrom("decastillo@dcatech.com");
           message.setText(email.getContenido());

          email.getReceptor().stream()
                  .forEach(contrato ->{
                              message.setTo(contrato.getEmpleado().getCorreoElectronico());
                  } );

           message.setSubject(email.getAsunto());

           this.emailSender.send(message);
       }catch (MailException e){
           e.getCause();
       }

    }
}
