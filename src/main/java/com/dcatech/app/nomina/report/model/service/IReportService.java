package com.dcatech.app.nomina.report.model.service;

import com.dcatech.app.nomina.report.model.entity.Nomina;
import com.dcatech.nomina.db.commons.models.entity.DetalleLiquidacion;
import com.dcatech.nomina.db.commons.models.entity.Liquidacion;
import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

public interface IReportService {





    String printReport( Nomina nomina) throws JRException, FileNotFoundException, SQLException;

}
