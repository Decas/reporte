package com.dcatech.app.nomina.report.exceptions.exceptions;

public class MasterNullException extends NullPointerException {
    public static final String DESCRIPCION = "El dato no puede ser nulo o vacío";

    public MasterNullException() {
        super(DESCRIPCION);
    }
}
