package com.dcatech.app.nomina.report.control;

import com.dcatech.app.nomina.report.model.entity.Email;
import com.dcatech.app.nomina.report.model.entity.Nomina;
import com.dcatech.app.nomina.report.model.service.IEmailService;
import com.dcatech.app.nomina.report.model.service.IReportService;
import com.dcatech.nomina.db.commons.models.entity.DetalleLiquidacion;
import com.dcatech.nomina.db.commons.models.entity.Liquidacion;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/reportes")
public class ReportController {
    @Autowired
    private IReportService service;

    @Autowired
    private IEmailService mail;

/*
    @PostMapping("/print")
    public List<Liquidacion> detalles(@RequestBody Nomina nomina){
        return this.service.showToPrint(nomina);
    }

 */




    @PostMapping("/getFile")
    public String print( @RequestBody Nomina nomina) throws FileNotFoundException, JRException, SQLException {
        return this.service.printReport( nomina);

    }

   /* @PostMapping("/email/send")
    public String enviar(@RequestBody Email email){
        this.mail.enviarMensaje(email);
        return "sucesss";

    }

    */

    @PostMapping("/email/send")
    public String enviar(@RequestBody Email email)throws MailException, MessagingException {

        this.mail.enviarMensajeAadjunto(email);
        return "sucesss";

    }

}
